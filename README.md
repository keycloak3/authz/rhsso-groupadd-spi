# Red Hat SSO Group Add SPI

This RHSSO (Keycloak) SPI will automatically grant group membership on registration. 


## Installing the SPI
 
To install the SPI, first build the jar using Maven:
`mvn clean install`

Then, copy the jar to `[RHSSO-ROOT]/standalone/deployments` and start the server. The server will
automatically detect the jar and deploy it. After it is deployed, it can be enabled for your realm
in the RHSSO admin dashboard by clicking `Events -> Config` adding `group-assignment-event-listener` 
in the `Event Listeners` box.

## How It Works

Auto role assigment rules are defined in src/main/resources/group-lookup.properties

Rules are in key value pair format. For example,

gov=Consumer
mil=Consumer

In the above example, newly registered user account with email address ending with .gov or .mil are auto-assigned to Consumer role in Keycloak. 
Consumer role must be pre-defined in Keycloak in order for this functionality to work. This SPI will not create a Consumer role for you. 

As new rules are defined, group-lookup.properties file must be updated and the SPI must be redeployed. In future iteration, this process could be automated. Further development is needed to enable automation.


